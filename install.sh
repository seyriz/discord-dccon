#!/usr/bin/env bash

echo "init virtualenv..."
python -m virtualenv venv

./venv/bin/pip install -r requirements.txt

cp nginx.conf /etc/nginx/