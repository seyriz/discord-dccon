import json
import time
import os
import glob

from requests import get


def download(url, filename):
    if glob.glob(os.path.join("static/dccon", filename + "*")):
        return None
    resp = get(url)
    mime = resp.headers.get("Content-Type")
    if mime == "image/gif":
        filename = "static/dccon/{}.gif".format(filename)
    elif mime == "image/png":
        filename = "static/dccon/{}.png".format(filename)
    elif mime == "image/jpg" or mime == "image/jpeg":
        filename = "static/dccon/{}.jpg".format(filename)
    with open(filename, "wb") as f:
        f.write(resp.content)
    return filename


def save_json():
    dccon = dict()
    for filename in os.listdir("static/dccon"):
        dccon[filename.split(".")[0]] = "/static/dccon/{}".format(filename)
    with open("static/dccon.min.json", mode='w', encoding="utf-8") as f:
        json.dump(dccon, f, ensure_ascii=False)
    with open("static/dccon.json", mode='w', encoding='utf-8') as f:
        json.dump(dccon, f, ensure_ascii=False, indent=4)

#
# with open('dccon_origin.json') as data_file:
#     dccon_list = json.load(data_file)
#     new_list = dict()
#     key_list = list(dccon_list.keys())
#
#     if not os.path.isdir("static"):
#         os.mkdir("static")
#     if not os.path.isdir("static/dccon"):
#         os.mkdir("static/dccon")
#
#     for key in key_list:
#         file = download(dccon_list.get(key), key)
#         if file:
#             new_list[key] = file
#             print("Dwonloaded: {} to {}".format(key, new_list.get(key)), end="")
#             time.sleep(2)
#         print(" Progress {}% {} of {}".format(key_list.index(key)/len(key_list)*100, key_list.index(key), len(key_list)))
#     save_json()


with open('dccon_origin.json', mode='r', encoding='utf-8') as origin:
    with open('static/dccon.json', mode='w', encoding='utf-8') as new:
        for l in origin.readlines():
            if l.count(":") > 0:
                fname, ext = os.path.splitext(l.replace(",", "").replace("\n", "").replace("\"", ""))
                ld = json.loads("{" + l.replace(",", "").replace("\n", "") + "}")
                key = list(ld.keys())[0]
                ld[key] = "/static/dccon/{}{}".format(key, ext)
                ls = json.dumps(ld, ensure_ascii=False)
                ll = ls.replace("{", "").replace("}", "") + ",\n"
                new.write(ll)
                print(ll)
            else:
                new.write(l)