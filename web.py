# TODO: Add DBMS
from flask import Flask, send_file, render_template, request, send_from_directory, cli

from dc_con import dc_con

app = Flask(__name__)


@app.route('/dc_con/<name>')
def dc_con(name):
    return send_file(dc_con.get_dccon(name))


@app.route('/list')
def dc_con_list():
    return send_file('templates/dccon_list.html')


@app.route('/get_json')
def dc_con_json():
    return send_file('static/dccon.json')


@app.route("/")
def index():
    return render_template("new_list.html")


@app.route('/dist/<filename>')
def webpack_dist(filename):
    return send_from_directory("static/dccon-selector/dist", filename)


@app.route("/public/<filename>")
def webpack_public(filename):
    return send_from_directory("static/dccon-selector/public", filename)

@app.cli.command("initialize")
def initialize():
    # TODO: DB initialize
    pass