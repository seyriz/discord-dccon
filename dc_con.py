
import json


class SingletonInstane:
  __instance = None

  @classmethod
  def __getInstance(cls):
    return cls.__instance

  @classmethod
  def instance(cls, *args, **kargs):
    cls.__instance = cls(*args, **kargs)
    cls.instance = cls.__getInstance
    return cls.__instance


class DcCon(SingletonInstane):

    dccon_list = dict()

    def __init__(self):
        with open('static/dccon.json') as data_file:
            self.dccon_list = json.load(data_file)

    def get_dccon(self, name):
        name = name.replace("~", "")
        return self.dccon_list.get(name)


dc_con = DcCon.instance()