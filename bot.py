# bot.py
import os
import json

import discord

from dc_con import dc_con

# TODO: File upload function

if os.environ.get("DCCON_DEBUG"):
    print("DEBUG")
    dccon_base_url = "http://localhost:5000"
else:
    print("PRODUCTION")
    dccon_base_url = "http://dccon.cultr.camp"


class CommandHandler:

    # constructor
    def __init__(self, client):
        self.client = client
        self.commands = []

    def add_command(self, command):
        self.commands.append(command)

    def command_handler(self, message):
        if message.content.startswith("~"):
            cmd = message.content.replace("~", "")
            dccon_listing_cmd = ["리스트", "목록"]
            dccon_adding_cmd = ["추가", "건의", "질문", "이슈"]
            dccon_help_cmd = ["", "도움말", "도움", "도움!", "help"]
            if dccon_listing_cmd.count(cmd):
                return client.send_message(message.channel, dccon_base_url + "/list")
            elif dccon_adding_cmd.count(cmd):
                return client.send_message(message.channel, "https://bitbucket.org/seyriz/discord-dccon/issues?status=new&status=open")
            elif dccon_help_cmd.count(cmd):
                return client.send_message(message.channel, "목록 보기: {}, 디시콘 추가요청: {}, 이 도움말 보기: {} \n Funzinnu의 디시콘 선택기에 영감을 받았습니다.".format(dccon_listing_cmd, dccon_adding_cmd, dccon_help_cmd))
            else:
                dccon = dc_con.get_dccon(cmd)
                if dccon:
                    if dccon.startswith("http"):
                        return client.send_message(message.channel, dccon)
                    else:
                        return client.send_message(message.channel, dccon_base_url + dccon)
                else:
                    return None


# create discord client
client = discord.Client()
token = 'NTIzNTUxODYwNTg2NDQ2ODU4.DvrTFA.Vbqnx9fT9EJ0M1dChmVILejQ_HM'
permission = 296960

# https://discordapp.com/oauth2/authorize?client_id=523551860586446858&scope=bot&permissions=522304
# https://discordapp.com/oauth2/authorize?client_id=523551860586446858&scope=bot&permissions=2048
# create the CommandHandler object and pass it the client
ch = CommandHandler(client)


# bot is ready
@client.event
async def on_ready():
    try:
        print(client.user.name)
        print(client.user.id)

    except Exception as e:
        print(e)


# on new message
@client.event
async def on_message(message):
    # if the message is from the bot itself ignore it
    if not (message.author == client.user):
        # try to evaluate with the command handler
        try:
            if message.content.startswith("~"):
                await ch.command_handler(message)
                # await ch.command_handler(message)

        # message doesn't contain a command trigger
        except TypeError as e:
            print("TypeError", e)
            pass

        # generic python error
        except Exception as e:
            print("Exception", e)


client.run(token)
