import Vue from 'vue'
import App from './App.vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.css'
import axios from 'axios'
import VueClipboard from 'vue-clipboard2'

Vue.use(Vuetify);
Vue.use(VueClipboard);
Vue.prototype.$http = axios;

new Vue({
  el: '#app',
  render: h => h(App)
});